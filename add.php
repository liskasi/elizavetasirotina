<!-- Adding a product page -->

<!DOCTYPE html>
<html lang="en">
    <head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>AddProducts</title>
	<!-- Bootstrap core CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        <link rel="stylesheet"  href="style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="js_magic.js"></script>

    </head>
        <body>
    <form id="addform" method="POST">
        <div id = "h1_buttons">
            <h1 id="heading_productList">Product Add</h1>
            <div>
            <!-- Save action to save a new product -->
                <a id="submitSave" class="btn btn-outline-success me-md-4">Save</a>
                <!-- Cancel action that cancels adding a new product -->
                <a href="cancel.php" id="reset_btn" class="btn btn-outline-danger me-md-4">Cancel</a>
            </div>
        </div>
        <hr id="hr1">

        <ul id="form_list">
            <li>
                <label for="sku">SKU</label>
                <input type="text" id="sku" name="sku">
            </li>
            <li>
                <label for="name">Name</label>
                <input type="text" id="name" name="name">
            </li>
            <li>
                <label for="price">Price ($)</label>
                <input type="text" id="price" name="price">
            </li>
            <li>
                <label for="dropdown_type">Type Switcher</label>
                <select name="TypeSw" id="dropdown_type">
                    <option label=" "></option>
                </select>
            </li>
            <li id="addingFields">
            </li>
        </ul>
        </form>
        <hr id="hr2">
        <footer>
            <p>Scandiweb Test assigment</p>
        </footer>

        <!-- Bootstrap Bundle with Popper -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>    
    </body>
</html>