var type_option = ["DVD", "Furniture", "Book"]; //array for Type Switcher

let attrType = [["Size","MB"], ["Dimensions",""],["Weight","KG"]]; // array for attribute type
var attributes =[["Size"],["Height","Width","Length"],["Weight"]]; // array for attribute names
var measures = [[" (MB)"],[" (CM)"," (CM)","  (CM)"],[" (KG)"]]; // array for attribute's measurements

var description = ["Please, provide size","Please, provide dimensions","Please, provide weight"]; // array for descriptions
//MAIN IDEA: depending on index of chosen attribute type we get elements from other arrays
var type; 
var index; 

$(function() {

    for(let i=0; i < 3; i++){
        $("#dropdown_type").append('<option value="'+type_option[i]+'">'+type_option[i]+'</option>');
    }


    $('#dropdown_type').click(function()
    {
        $("#addingFields").empty();

        type = $('#dropdown_type').val();
        index = type_option.indexOf(type); //we get index here

        console.log(measures[index][0]);

        let len = attributes[index].length-1; // we need this variable so after last element we will print out the description
//appending first label with input field
        $("#addingFields").append($("<li>",{
            id: "addingFields_li"+attributes[index][0],
        }))
        $("#addingFields_li"+attributes[index][0]).append($("<label>",{
            id: "attr_ap",
            for: ""+attributes[index][0],
            text: ""+attributes[index][0]+""+measures[index][0]
        }))
        console.log(attributes[index][0]+""+measures[index][0]);
        $("#addingFields_li"+attributes[index][0]).append($("<input>",{
            id: "Attribute",
            name: "Attribute"
        }))
//this will work for Furniture to add other two labels with input fields
        for(let j = 1; j < attributes[index].length; j++)
        {
            $("#addingFields").append($("<li>",{
                id: "addingFields_li"+attributes[index][j],
            }))
            $("#addingFields_li"+attributes[index][j]).append($("<label>",{
                id: "attr_ap"+attributes[index][j],
                for: ""+attributes[index][j],
                text: ""+attributes[index][j]+""+measures[index][j]
            }))
            $("#addingFields_li"+attributes[index][j]).append($("<input>",{
                id: "Attribute"+attributes[index][j],
                name: "Attribute"
            }))
        }
//appending description
        $("#addingFields_li"+attributes[index][len]).append($("<p>",{
            id: "desc",
            text: ""+description[index]
        }))

    });

    $('#submitSave').click(function()
    {   
        //different types of errors
        $errorEmpty = "Please, submit required data";
        $errorType = "Please, provide the data of indicated type";
        $errorE = 1;
        $errorT = 1;
        $error = 1;

        $sku = $("#sku").val();
        $name = $("#name").val();
        $price = $("#price").val();
        $attr = $("#attribute").val();

            if (!(($sku!="")&&($name!="")&&($price!="")))
            {   
                // alert($errorEmpty);
                $errorE = 0;
            }
            else 
            {
                //$.ajax({
                //     type: "POST",
                //     url: "check_unique.php",
                //     data: {
                //         sku:$sku,
                //     },
                //     success: function(data){
                //         if(!(parseInt(data)))
                //         {
                //             console.log(parseInt(data));
                //             $error = 0;
                //             alert("This SKU already exists. Please, input another SKU. Remember that it should be unique for each product!");
                //         }
                //     }
                // });
            
        
                if(isNaN($price)) 
                {
                    // alert($errorType);
                    $errorT = 0;
                }

//for Furniture
                if(index == 1)
                {
                    if(!(($("#Attribute").val() != "") && ($("#AttributeWidth").val() != "") && ($("#AttributeLength").val()!="")))
                    {
                        // alert($errorEmpty);
                        $errorE = 0;
                    }
                    else if (isNaN($("#Attribute").val())|| (isNaN($("#AttributeWidth").val()) || (isNaN($("#AttributeLength").val()))))
                    {
                        // alert($errorType);
                        $errorT = 0;
                    }
                    else
                    {
                        $("#AttributeLength").val($("#Attribute").val()+"x"+$("#AttributeWidth").val()+"x"+$("#AttributeLength").val());
                        $attr=$("#AttributeLength").val();
                    }
                }
                else
                {
                    if($("#Attribute").val() == "")
                    {
                        // alert($errorEmpty);
                        $errorE = 0;
                    }
                    else if (isNaN($("#Attribute").val()))
                    {
                        // alert($errorType);
                        $errorT = 0;
                    }
                    else
                    {
                        $attr=$("#Attribute").val();
                    }
                }
                $attr=attrType[index][0]+": "+$attr+" "+attrType[index][1];
            }
            //checking if there are any errors
            if($errorT == 0)
            {
                alert($errorType);
                $error=0;
            }
            else if ($errorE == 0)
            {
                alert($errorEmpty);
                $error=0;
            }


            if($error)
            {
                $.ajax({
                    type: 'POST',
                    url: 'post.php',
                    data:
                    {
                        sku:$sku,
                        name:$name,
                        price:$price,
                        attribute:$attr
                    },
                    success: function()
                    {
                        window.location.href = "index.php";
                    }
                })
            }
        
    });
});