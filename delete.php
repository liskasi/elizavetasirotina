<?php
namespace public_html;

require('products_class.php');

$products = new Products();

//If checkbox are selected, each selected element will be deleted, otherwise a user will be redirected to the Product list page
if(isset($_POST["checkbox"]))
{
    $check = $_POST["checkbox"];
    for($i = 0; $i < count($_POST['checkbox']); $i++)
    {
        $product_checkbox = $_POST['checkbox'][$i];
        $products->delete($product_checkbox);
    }
}
else
{
    header("location: index.php");
}