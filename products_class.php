<?php
namespace public_html;

require('dbh_class.php');
use PDO;


class Products extends dbh
{
    // function is used to select products from the database
    public function getProduct()
    {
        $statement = $this->connect()->prepare("SELECT * FROM `product`");
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    // function is used to add a product to the database
    public function addProduct($sku,$name,$price,$attr)
    {
        $sql = "INSERT INTO `product`(`SKU`, `Name`, `Price`, `Attribute`) VALUES (?,?,?,?)";

        $statement=$this->connect()->prepare($sql);
        $statement->execute([$sku,$name,$price,$attr]);
        header("location: index.php");
    }

    // function that redirects us to Product list page
    public function cancel()
    {
        header("location: index.php");
    }
    // function that deletes a product from the database
    public function delete($sku)
    {
        $sql = "DELETE FROM `product` WHERE `SKU`='$sku';";
        $statement=$this->connect()->prepare($sql);   
        $statement->execute();
        header("location: index.php");
    }
    // function that checks if there is already a product with a same SKU 
    public function select($sku)
    {
        $sql = "SELECT COUNT(*) as c FROM `product` WHERE `SKU` = '$sku';";
        $statement=$this->connect()->prepare($sql);   
        $statement->execute();

        $total = $statement->fetch(PDO::FETCH_ASSOC);

        $total_m= $total['c'];
        return $total_m;
    }
}
