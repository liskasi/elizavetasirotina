<?php 
namespace public_html;

require('products_class.php');
?>

<!DOCTYPE html>
<html lang="en">
    <head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="Elizaveta Sirotina">
    
	<title>Products</title>
	<!-- Bootstrap core CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        <link rel="stylesheet"  href="style.css">
    </head>
        <body>
        <form method="POST" action="delete.php">

            <div id = "h1_buttons">
                <h1 id="heading_productList">Product List</h1>
                <div>
                <!-- “ADD” leads to the “Product Add” page -->
                    <a href="add.php" class="btn btn-outline-success me-md-4">ADD</a>
                <!-- “MASS DELETE” deletes selected products -->
                    <button class="btn btn-outline-danger me-md-4">MASS DELETE</button>
                </div>
            </div>
            <hr id="hr1">
            <main>
                <div class="grid-container">
                <?php $products = new Products();?>
                <?php foreach ($products->getProduct() as $prod): ?>
                    <div class="item">
                        <ul class="prod_det">
                            <li><input type="checkbox" value="<?php echo $prod['SKU']; ?>" name="checkbox[]" class="box_prod"></li>
                            <li><?php echo $prod['SKU'] ?></li>
                            <li><?php echo $prod['Name'] ?></li>
                            <li><?php echo number_format($prod['Price'],2) ?> $</li>
                            <li>
                                <span id="attrType1"></span>
                                <span id="attrType"> <?php echo $prod['Attribute'] ?> </span>
                                <span id="attrType2"></span>
                            </li>
                        </ul>
                    </div>
                <?php endforeach; ?>
                </div>
            </main> 
        </form>


        <hr id="hr2">
        <footer>
            <p>Scandiweb Test assigment</p>
        </footer>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
        
    </body>
</html>